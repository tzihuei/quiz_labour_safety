1. ( 4 )承攬人發生職業災害時,可能產生的責任有 1 僅有行政責任 2僅有刑事責任 3 僅有民事責任 4 前三者都有可能。
2. ( 1 )事業單位雙方具有對等關係之契約為1承攬契約2雇傭契約3勞動契約4委任契約。
3. ( 4 )事業單位以其事業招人承攬時,承攬人僅負何種責任 1 職業安全衛生法 2勞動基準法 3 民法 4 以上都有可能。
4. ( 3 )事業單位與承攬人分別僱用勞工共同作業,未設置協議組織時,應受何種處分1 3 千元以下 2 3 萬元以下 3 3~15 萬元 4 3~30 萬元 罰鍰。
5. ( 1 )承攬契約的報酬給付之認定依據 1 完成工作時 2 有提供勞務行為 3 工作或有瑕疵 4 以上均是。
6. ( 1 )事業單位未將工作場所之危害因素告知承攬人,致承攬人之勞工因而發生災害時,受害勞工可以向原事業單位請求1民事賠償2國家賠償3職業災害補償4勞工保險給付。
7. ( 1 )原事業單位對於承攬人之危害告知方式為下列之一 1 書面告知 2 口頭告知3 現場告知 4 以上皆是。
8. ( 4 )受領勞務與提供勞務雙方具有獨立人格者為 1 派遣契約 2 雇傭契約 3 勞動契約 4 承攬契約。
9. ( 3 )事業單位未將工作場所之危害因素告知承攬人時,應受何種處分 1 3 千元以下罰鍰 2 18 萬元以下罰金 3 3~15 萬元罰鍰 4 3~30 萬元罰鍰。
10. ( 2 )勞動契約與承攬契約是不同的契約行為,勞動契約之報酬給付認定方式 1 完成工作時 2 有提供勞務行為 3 工作或有瑕疵 4 以上均是。
11. ( 4 )所謂『工作者』指1勞工 2自營作業者 3其他受工作場所負責人指揮或監督從事勞動之人 4以上皆是。
12. ( 2 )勞動契約存續中,由雇主所提示,使勞工履行契約提供勞務之場所係指1工作場所 2勞動場所 3就業場所 4作業場所。
13. ( 1 )雇主宜設置經『型式檢定』合格之機械器具供勞工使用,不得低於機械器具安全防護標準者為1堆高機 2鍋爐 3移動式起重機 4升降機。
14. ( 1 )依法令規定,下列何者為危險性機械? 1固定式起重機吊升荷重在三公噸以上 2壓力容器 3高壓氣體特定設備 4高壓氣體容器。
15. ( 3 )高溫作業勞工作息時間標準所稱高溫作業場所,應每 1一 2二 3三 4六 個月測定綜合溫度熱指數一次以上。
16. ( 4 )經審查合格之下列何種工作場所,於施工過程中變更主要分項工程施工方法時,應就變更部分重新評估? 1甲類 2乙類 3丙類 4丁類。
17. ( 4 )設有中央管理方式之空氣調節設備之建築物室內作業場所,應每1一 2二 3三 4六個月測定二氧化碳濃度一次以上。
18. ( 4 )具有特殊危害之作業,下列何者為非?1高溫作業 2異常氣壓作業 3高架作業 4噪音作業。
19. ( 1 )雇主於僱用一般作業之勞工時,應施行1體格檢查 2定期健康檢查 3特殊體格檢查 4特殊健康檢查。
20. ( 3 )勞工有配合接受之義務,下列何者為非?1應遵守安全衛生工作守則 2接受安全衛生教育訓練之義務 3急救人員 4接受體格檢查、健康檢查違反者可處新台幣 3000 元以下罰鍰。
21. ( 1 )事業單位以其事業之全部或一部分交付承攬時,應於事前以1書面 2口頭 3錄影 4照相 為之。
22. ( 4 )設置協議組織,並指定工作場所負責人,擔任指揮及協調之工作;工作場所負責人應由何人指派擔任? 1勞工 2承攬人 3領班 4原事業單位。
23. (3)依職業安全衛生法規,事業單位與承攬人,再承攬人分別僱用勞工共同作業時,應由何者指定工作場所負責人,擔任統一指揮及協調工作?1承攬人2在承攬人3原事業單位4檢查機構。
24. ( 3 )有關事業單位訂定安全衛生工作守則之規定為何錯誤 1 應依職業安全衛生法及有關規定訂定適合其需要 2 會同勞工代表訂定 3 應報經地方主管機關備查 4 報經檢查機構備查後公告實施。
25. ( 1 )依職業安全衛生法規定新雇用勞工時應施行 1 體格檢查 2 定期健康檢查3 特殊健康檢查 4 其他經中央主管機關指定之健康檢查。
26. ( 3 )依法令規定,事業單位勞動場所發生職業災害,應於 8 小時內通報勞動檢查機構者,下列何者為非? 1 死亡災害 2 災害之罹災人數在 3 人以上 3 災害嚴重率在 2 以上 4 災害之罹災人數為 1 人以上,且需住院治療。
27. ( 3 )事業單位工作場所發生死亡災害時下列敘述何者為非 1 8 小時內報告檢查機構 2 施以必要之急救與搶救 3 在每個月填載職業災害統計報告時才報告機構 4 雇主非經司法機關或檢查機構許可,不得移動或破壞現場。
28. ( 3 )依職業安全衛生法施行細則規定,僱用勞工人數在多少人以上,僱主應按月填載職業災害統計,報請勞動檢查機構備查?(1)55 (2) 130 (3) 50 (4) 100。
29. ( 1 )依職業安全衛生法規定,中央主管機關指定之事業,雇主應多久填載職業災害統計,報請勞動檢查機構備查?1每月2每三個月3每半年4每年。
30. ( 4 )依職業安全衛生法當勞工違反法令得處三千元以下罰鍰,其應遵守及配合事項下列敘述何者正確 1 遵守工作守則 2 接受教育訓練 3 配合健康檢查 4以上皆是。
31. ( 4 )工作場所有立即發生危險之虞時,如何處理 1 報告檢查機構 2 報告主管機關 3 繼續作業 4 由工作場所負責人下令立即停工,並使勞工退避至安全場所。
32. ( 1 )依法令規定,勞工執行職務發現有立即發生危險之虞者,得在不危及其他工作者安全情形下,採取何種措施?1 自行停止作業及退避至安全場所,並立即向直屬主管報告 2 立即下令工作場所停工 3 無須告知主管,並儘速離開工作場所 4 留在原地不動,等待救援。
33. ( 3 )危險性機械或設備不包含下列何項 1 人字臂起重桿 2 高壓氣體特定設備3 堆高機4 鍋爐。
34. ( 2 )高溫作業場所,每日不得超過 1 4 2 6 3 8 4 10 小時。
35. ( 2 )依法令規定,會同訂定安全衛生工作守則及參與實施職業災害調查分析之勞工代表的推派或推選,依優先順序,其第一優先為下列何者? 1 由勞資會議之勞方代表推選 2 由工會推派 3 由全體員工推選 4 由雇主派任。
36. ( 4 )依法令規定,事業單位工作場所發生職業災害,應由下列何者會同勞工代表實施調查、分析及作成紀錄? 1 勞動檢查機構 2 警察局 3 縣市政府 4 雇主。
37. ( 3 )依法令規定,雇主不得使未滿幾歲者,從事坑內工作等危險性或有害性工作?(1) 6 (2) 12 (3) 18 (4) 20。
38. ( 3 )依法令規定,經中央主管機關指定具有危險性之機械或設備操作人員,雇主應僱用經由中央主管機關認可之訓練或技能檢定合格之人員充任之,如違反規定會受何種處罰? (1) 處新臺幣 1,000 元以下罰鍰 (2) 處新臺幣 5,000 以下罰鍰 (3) 處新臺幣 3 萬元以上 30 萬元以下罰鍰 (4) 處 1 年以下有期徒刑拘役、科或併科新台幣 5,000 元罰金。
39. ( 2 )違反職業安全衛生法規定之重大職業災害通報義務,處: (1) 一年以下有期徒刑、拘役、科或科新臺幣 9 萬元以下罰金 (2) 新台幣 3~30 萬元以下罰鍰 (3) 新台幣 3~15 萬元以下罰鍰 (4) 新臺幣 3~6 萬元以下罰鍰。
40. ( 3 )依職業安全衛生法施行細則規定,事業單位之職業安全衛生管理由何人負執行之責? 1職業安全衛生管理單位業務主管 2 職業安全衛生委員會執行秘書 3 事業各部門主管 4 職業安全衛生管理人員。
41. ( 3 )依職業安全衛生法規定,事業單位以其事業招人承攬時,其承攬人就承攬部分負本法所定雇主之責任,而下列何者應就職業災害補償與承攬人負連帶責任?1 勞動檢查機構 2 縣市政府 3 原事業單位 4 勞工保險局。
42. ( 4 )應通報中央主管機關之作業環境監測計畫及結果,經中央主管查核發現有虛偽不實者,處罰鍰:1 3~15 萬元以下 2 3~30 萬元以下 3 10~100 萬元以下 430~100 萬元以下。
43. ( 3 )職業安全衛生法之中央主管機關為下列何者? 1 內政部 2 行政院經濟建設委員會 3 勞動部 4 行政院衛生署。
44. ( 3 )事業單位如將其事業交付承攬,其勞工人數之計算,下列何者正確? 1 事業單位與承攬人勞工人數個別計算 2 事業單位與承攬人勞工人數合併計算 3 事業單位與承攬人勞工如於同一期間,同一工作場所作業時,合併計算總人數 4 人數之計算應報請當地勞動檢查機構認定。
45. ( 2 )依職安法規定,新化學物質未經許可登記或管制性化學品未經許可,而有製造、輸入、供應之行為者,處罰鍰: 1 30~300 萬元以下 2 20~200 萬元以下3 10~100 萬元以下 4 3~30 萬元以下。
46. (4)職業安全衛生法之目的,在於保障工作者: (1)生命財產(2)賺錢(3)勞資合諧(4)安全與健康。
47. (4)職業安全衛生法的適用行業有(1)16 項(2)15 項(3)13 項(4)各業。
48. (2)我國最高勞工行政主管機關為? (1)內政部(2)勞動部(3)台北市政府勞工局(4)高雄市政府勞工局。
49. (4)於勞動契約續存中,由雇主所提示,使勞工履行契約提供勞務之場所,為職業安全衛生法所稱? (1)職業場所(2)工作場所(3)作業場所(4)勞動場所。
50. (4)工作場所有立即發生危險之虞時,如何處理? (1)報告查機構(2)報告主管機關(3)繼續工作(4)由現場負責下令立即停工,並使勞工退避至安全場所。
51. (1)事業單位工作場所發生勞工死亡或罹難人數在三人以下之職業災害,雇主非經司法機關或勞動檢查機構許可,移動或破壞現場,將受何種處罰? (1)一年以下有期徒刑、拘役、科罰金或併科新台幣十八萬元以下罰金(2)新台幣十五萬元以下罰鍰(3)停工(4)新台幣六萬元以下罰鍰。
52. (3)事業單位工作場所發生災害時,下列敘述何者為非? (1)8 小時報告檢查(2)施以必要之急救與搶救(3)在每個月填載職業該害統計報告才報告機構(4)雇主非經司法機關或檢查機後許可,不得移動或破壞現場。
53. (1)下列哪一種機械、器具設置時,應符合職業安全衛生法之中央主管機關所訂之防護標準(1)動力衝剪機械(2)車床(3)鑚床(4)牛頭刨床。
54. (1)依法令規定,事業單位工作場所發生勞工死亡災害時,雇主應於多少時間內報告勞動檢查機構? (1)八小時(2)二十四小時(3)四十八小時(4)五日,
55. (2)目前我國中央勞工行政主管機關為? (1) 內政部 (2) 勞動部 (3) 行政院經濟建設委員會 (4) 台閩地區勞工保險局。
56. (3)雇主已依法令規定提供強烈噪音安全工作場所勞工聽力防護具,但未監督勞工確實使用時,顯已違法下列何種法律規定? (1)勞動基準法(2)勞動檢查法(3)職業安全衛生法(4)勞工保險條例。
57. (3)依職業安全衛生規定,事業單位應多久時間填寫報職業統計報表一次? (1)每週(2)每半個月(3)每月(4)每季。
58. (1)違反職業安全衛生法第六條第一項,導致「事業單位工作場所發生死亡之職業災害,雇主將受何種處罰? (1)三年以下有期徒刑拘役或併科新台幣三十萬元以下罰金(2)一年以下有期徒刑拘役或併科新台幣十八萬元以下罰金(3)停工(4)新台幣六萬元以下之罰鍰。
59. (2)依職業安全衛生法規定,下列何種情形得處三年以下有期徒刑(1)雇主僱用勞工時未施行體格檢查(2)鍋爐超過規定使用期間未經再檢查而繼續使用而發生死亡之職業災害(3)未設置安全衛生組織或管理人員(4)未對勞工施以從事工作必要之安全衛生教育訓練及預防災變之訓練。
60. (4)勞工如發現事業單位違反職業安全衛生法規令規定時,依法令規定其申訴對象不包括下列何種? (1)雇主(2)主管機關(3)勞動檢查機構(4)目的事業主管機關。
61. (4)依職業安全衛生法令規定,下列何種屬於危險性設備? (1)固定式起重機(2)升降機(3)營建用提升機(4)鍋爐。
62. (3)依職業安全衛生法施行細則規定,下列何者不屬具有危險性之設備?1鍋爐2壓力容器3電器設備4高壓氣體特定設備。
63. (2)依職業安全衛生法規定雇主對勞工施以從事工作所必要安全衛生教育及預防災變訓練,經通知限期改善,屆期未改善,違反者應受何種處罰? (1)處新台幣三千元以下罰鍰(2)處三萬元以上十五萬元以下罰鍰(3)處三萬元以上三十萬元以下罰鍰(4)處一年以下有期徒刑拘役或併科九萬元以下罰金。
64. (2)依職業安全衛生法規在高溫場所工作之勞工雇主不得使其每日工作時間超過? (1)5 (2)6 (3)7(4)8 小時。
65. (1)依職業安全衛生法規定,勞工不接受法定之安全衛生教育訓練者,處以下列何種處罰? (1)處新台幣三千元以下之罰鍰(2)處新台幣三千元以上之罰金(3)處新台幣六千元以下之罰款(4)處新台幣六千元以下之罰金。
66. (3)職業安全衛生法令規定,勞工工作場所之建築物,應由依法登記開業之何種技術有計? (1)工業安全技師(2)工業衛生技師(3)建築師(4)土木技師。
67. (3)某縣有一事業單位因違反職業安全衛生法規,經當地檢查機構與以罰鍰處分,該單位如有不服,得依法向機關提起訴願? (1)當地縣政府(2)台灣省政府(3)勞動部(4)行政院。
68. (2)勞工健康檢查所需費用應由下列何者負擔? (1)勞工(2)雇主(3)勞工保險局(4)勞資雙方各半。
69. (2)特別危害健康作業勞工,於受雇時,應實施下列何種檢查? (1)一般體格檢查(2)特殊體格檢查(3)一般及特殊體格檢查(4)特殊健康檢查。
70. (3)事業單位工作場所發生職業災害時,由雇主應會同何人實施調查、分析及統計工作? (1)檢查機構(2)司法機構(3)勞工代表(4)主管機關。
71. (3)事業單位有發生立即危險之虞時(1)報告董事長(2)報告檢察機關(3)應立即停工,並將工作人員退避至安全處所(4)為了達成生產目標影響繼續工作。
72. (1)依職業安全衛生法規定新雇勞工時應施行?(1)體格檢查(2)定期健康檢查(3)特殊健康檢查(4)其他經中央主管機關指定之健康檢查。
73. (2)通知停工之事業單位,依法令規定勞動檢查機構未允許復工前,若私自復工,可對其處以何種罰則? (1)罰鍰(2)有期徒刑、拘役、或科或科罰金(3)以函通知改善(4)移送法院強制執行。
74. (2)雇主未依法規定填報職業該害統計,經檢查機構通知改善而不如期改善者,依職業安全衛生法規定雇主將受何種處罰? (1)一年以下有期徒刑、拘役或科併科新台幣九萬元以下罰鍰(2)新台幣三萬元以上十五萬元以下罰鍰(3)新台幣三萬以上六萬以下罰鍰(4)新台幣三千元以下罰鍰。
75. (4)下列有關事業單位工作場所發生勞工死亡職業災害之處理之敘述,何者錯誤? (1)事業單位應即採取必要措施(2)非經許可不得移動或破壞現場(3)應於八小時內報告檢查機構(4)於當月職業該害統計月報表陳報者,得免八小時內報告檢查機構。
76. (3)政府實施安全衛生檢查的機構為? (1)勞工局(2)警察局(3)勞動檢查機構(4)勞保局。
77. (4)訂定安全衛生工作守則時,下列敘述何者錯誤? (1)要有勞工代表會同訂定(2)訂定要經檢查機構備查(3)屬於雇主責任不得轉嫁給勞工(4)只要合理即可不需考慮其可行性。
78. (2)下列何者不屬於職業安全衛生法所指之職業災害? (1)勞工於噴漆時有機劑中毒(2)化學工廠爆炸致居民死傷多人(3)勞工因工作罹患疾病(4)勞工修理機器感電死亡。
79. (4)事業單位所訂定之安全衛生工作守則,其適用區域跨越省(市)時應報經: (1)省政府勞工處(2)市政府勞工局(3)省政府勞工處及市政府勞工局(4)中央主管機關報備。
80. (2)於勞動場所中,接受雇主或代理雇主指示處理有關勞工事務之人所能支配、管理之場所,為職業安全衛生法所稱之何種場所?(1)職業職場(2)工作場所(3)作業場所(4)就業場所。
81. (4)勞工因職業上原因不能適應原有工作時,雇主應採取之措施何者有誤: (1)變更勞工工作場所(2)縮短其工作時間(3)予以醫療(4)予以解雇。
82. (1)勞工不遵守工作守則之處罰,是由:(1)主管機關(2)雇主(3)法院(4)警察機關處分。
83. (3)職業安全衛生法之中央主管機關為下列何者? (1) 內政部 (2) 行政院經濟建設委員會 (3)勞動部 (4) 行政院衛生署。
84. (2)雇主訂定適合其需要之安全衛生工作守則,應會同何人辦理? (1)職業安全衛生人員(2)勞工代表(3)電器技術人員(4)醫療人員。
85. (3)經中央主管機關指定具有危險性之機械或設備操作人員,雇主應僱用經中央主管機關認可之訓練或技能檢定合格之人士充任之,如違反規定會受何種處罰? (1)處新台幣一仟元以下罰鍰(2)處新台幣五千元以下罰鍰(3)處新台幣三萬元以上三十萬元以下罰鍰(4)處一年以下有期徒刑、拘役、科或併科新台幣五千元。
86. (1)事業單位僱用勞工人數多少人以上規定設職業安全衛生管理單位或人員時,應於事業開始之日填具「職業安全衛生管理單位(人員)設置報備書」呈報當地檢查機構備查? (1)30 (2)60 (3)100 (4)300 。
87. (4)事業單位僱用勞工人數幾人以上,應依事業單位性質訂定工作守則 (1)100 (2)300 (3)30(4)只要雇用勞工皆須報備人 。
88. (3)依法令規定,職業安全衛生業務主管,每 2 年應至少接受幾小時之安全衛生在職教育訓練? (1)1 (2)3 (3)6 (4)12。
89. (3)事業單位新進勞工之安全教育訓練時數最少不得低於?(1)六 (2)四 (3)三 (4)二 小時。
90. (2)職業安全衛生法規定之事業單位勞工人數在多少人以上,應僱用或特約醫護人員,辦理健康管理、職業病預防等健康保護事項? (1)30 (2)50 (3)100 (4)300 。