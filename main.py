import argparse
import random
import re
import sys


DATA_SOURCES = [
    {"path": "./data/chapter_1-1.txt", "topic": "企業經營風險與安全衛生", "chapter": "1-1"},
    {"path": "./data/chapter_1-2-1.txt", "topic": "職業安全衛生相關法規/職業安全衛生法與施行規則", "chapter": "1-2-1"},
    {"path": "./data/chapter_1-2-2.txt", "topic": "職業安全衛生相關法規/勞動檢查法及其他相關法規", "chapter": "1-2-2"},
    {"path": "./data/chapter_1-2-3.txt", "topic": "職業安全衛生相關法規/職業災害勞工保護法", "chapter": "1-2-3"},
    {"path": "./data/chapter_1-2-4.txt", "topic": "職業安全衛生相關法規/職業安全衛生設施規則", "chapter": "1-2-4"},
    {"path": "./data/chapter_1-2-5.txt", "topic": "職業安全衛生相關法規/職業安全衛生管理辦法", "chapter": "1-2-5"},
    {"path": "./data/chapter_1-2-6.txt", "topic": "職業安全衛生相關法規/危害性化學品標示及通識規則", "chapter": "1-2-6"},
    {"path": "./data/chapter_1-3.txt", "topic": "職業安全衛生概論", "chapter": "1-3"},
    {"path": "./data/chapter_2-1.txt", "topic": "職業安全衛生管理系統介紹", "chapter": "2-1"},
    {"path": "./data/chapter_2-2.txt", "topic": "風險評估", "chapter": "2-2"},
    {"path": "./data/chapter_2-3.txt", "topic": "承攬管理", "chapter": "2-3"},
    {"path": "./data/chapter_2-4.txt", "topic": "採購管理", "chapter": "2-4"},
    {"path": "./data/chapter_2-5.txt", "topic": "緊急應變管理(含急救)", "chapter": "2-5"},
    {"path": "./data/chapter_3-1.txt", "topic": "墜落危害預防管理實務", "chapter": "3-1"},
    {"path": "./data/chapter_3-2.txt", "topic": "機械安全管理實務", "chapter": "3-2"},
    {"path": "./data/chapter_3-3.txt", "topic": "火災爆炸預防管理實務", "chapter": "3-3"},
    {"path": "./data/chapter_3-4.txt", "topic": "感電危害預防管理實務", "chapter": "3-4"},
    {"path": "./data/chapter_3-5.txt", "topic": "塌崩塌危害預防管理實務", "chapter": "3-5"},
    {"path": "./data/chapter_3-6.txt", "topic": "化學性危害預防管理實務", "chapter": "3-6"},
    {"path": "./data/chapter_3-7.txt", "topic": "物理性危害預防管理實務", "chapter": "3-7"},
    {"path": "./data/chapter_3-8.txt", "topic": "健康管理與促進", "chapter": "3-8"},
    {"path": "./data/chapter_3-9.txt", "topic": "職業災害調查處理與統計", "chapter": "3-9"},
    ]
QUIZ_MODE = {"standard": 80, "medium": 50, "small": 40, "lite": 20, "test": 3}


def yield_question(file_path):
    with open(file_path) as fobj:
        for sentence in fobj:
            m = PAT.search(sentence)
            try:
                number = m.group(1)
                answer = m.group(2)
                question = m.group(3)
                yield number, answer, question
            except Exception:
                print(sentence)

class QuizFactory:
    def __init__(self, data_sources, num_quiz):
        self.data_sources = data_sources
        self.num_quiz = num_quiz
        self.quiz_pattern = re.compile("^(\d+).\s*\(\s*(\d+)\s*\)([\w\W]*)")
        self.quiz_pool = self._get_quiz_pool()

    def _yield_quiz_from_sources(self, file_path):
        with open(file_path) as fobj:
            for sentence in fobj:
                m = self.quiz_pattern.search(sentence)
                try:
                    number = int(m.group(1))
                    answer = int(m.group(2))
                    quiz = m.group(3)
                    yield number, answer, quiz
                except Exception:
                    print(file_path)
                    print(sentence)
    
    def _get_quiz_pool(self):
        quiz_pool = []
        for source in self.data_sources:
            for number, answer, quiz in self._yield_quiz_from_sources(source["path"]):
                quiz_info = {"quiz": quiz,
                             "answer": answer,
                             "info": f"Chapter {source['chapter']}: {source['topic']} - #{number}"}
                quiz_pool.append(quiz_info)
        return quiz_pool

    def yield_quiz(self):
        pool = random.sample(self.quiz_pool, k=self.num_quiz)
        for quiz in pool:
            yield quiz["quiz"], quiz["answer"], quiz["info"]

def comment_print(s):
    print("================================================================================")
    print(s)


def show_welcome_message(mode):
    if mode not in QUIZ_MODE:
        print("mode must be one of standard/medium/small/lite/test")
        sys.exit(0)

    print("*****************************\n歡迎來到甲種職業安全衛生業務主管模擬測驗\n********************************")
    num_quiz = QUIZ_MODE[mode]
    continued = input(f"以下將有{num_quiz}題選擇題，滿分100分，及格60分，請問你要繼續嗎？ (y/n)")
    if not continued.lower() == "y":
        print("See you next time!")
        sys.exit(0)

    print("\n")
    return num_quiz

        
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", default="standard",
                        help="standard (80 quiz)/medium (50 quiz)/small (40 quiz)/lite (20 quiz)")

    arg = parser.parse_args()
    return arg


def main():
    arg = parse_args()
    num_quiz = show_welcome_message(arg.mode)
    quiz_factory = QuizFactory(DATA_SOURCES, num_quiz)

    n = 0
    credit = 0

    for quiz, answer, info in quiz_factory.yield_quiz():
        n += 1
        print(f"{n}. " + quiz)
        print("your answer: ", end=" ")
        my_answer = input()
        if my_answer.isdigit() and int(my_answer) == answer:
            credit += 1
            comment_print(f"correct! credit: {credit}/{quiz_factory.num_quiz} ref: {info}\n")
        else:
            comment_print(f"sorry, the answer is {answer}! ref: {info}\n")

    print("\nCongratulations! You have done the test!\n")
    print("****************************************************************************")
    print("****************************************************************************")
    print(f"credit: {credit}/{quiz_factory.num_quiz}")
    score = 100 * credit / quiz_factory.num_quiz
    is_pass = "PASS" if score >= 60 else "FAILED"
    print(f"score: {score}")
    print(f"result: {is_pass}")
    print("****************************************************************************")
    print("****************************************************************************")


if __name__ == "__main__":
    main()
