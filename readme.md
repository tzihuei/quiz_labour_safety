# 甲種職業安全衛生業務主管證照模擬考

## How to start?

```
>> python main.py -m (standard/medium/small/lite/test)
```

Argument:

-   -m (--mode) [standard<default></default>/medium/small/lite/test)]: 測驗題目，standard 會跟正式考試一樣，有 80 題，test 會有 3 題。

## 作答方式

-   每題都是選擇題，滿分 100
-   60 分及格
